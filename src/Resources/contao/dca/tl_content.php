<?php

$GLOBALS['TL_DCA']['tl_content']['list']['operations']['megalement'] = [
    'label' => ['MEGALEMENT','MEGALEMENT BEARBEITEN'],
    'href' => 'do=article&table=tl_content&megalement=1',
    'icon' => 'header.svg',
    'button_callback' => function($row, $href, $label, $title, $icon, $attributes) {
        return $row['type'] == 'megalement'
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : '';
    }
];

$GLOBALS['TL_DCA']['tl_content']['fields']['type']['save_callback'][] = function($value, \Contao\DataContainer $dc) {
    if ($value == 'megalement') {
        Contao\Database::getInstance()->prepare('INSERT IGNORE INTO tl_megalement(id, pid) VALUES(?, ?)')->execute($dc->activeRecord->id, $dc->activeRecord->id);
    }
    return $value;
};

if (\Contao\Input::get('megalement') == '1') {
    $GLOBALS['TL_DCA']['tl_content']['config']['ptable'] = 'tl_megalement';
}
