<?php

namespace Patrickjde\Contao\MegalementsBundle\Elements;

use Contao\ContentElement;

class ContentMegalement extends ContentElement
{
    protected $strTemplate = 'ce_html';

    protected function compile()
    {
        $strContent = '';

        $objElements = \ContentModel::findPublishedByPidAndTable($this->id, 'tl_megalement');

        if ($objElements !== null)
        {
            while ($objElements->next())
            {
                // Controller::getContentElement()
                $strContent .= $this->getContentElement($objElements->id);
            }
        }

        $this->Template->html = $strContent;
    }
}
